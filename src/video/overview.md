# Video

## Hosting

### PeerTube

https://joinpeertube.org/

It's not one website, but a platform which might be used on different websites.
You can upload to one such as https://libremedia.video

## Synchronized Social Viewing

Screen videos to view with friends. These require everyone to have access on their own. If you need to stream something from your desktop, you can just use the "share desktop" on [Jitsi meet](#jitsi).

Similar to: Rabbit, Netflix Party, Kast

### Metastream

https://getmetastream.com/

### OpenTogetherTube

https://opentogethertube.com/
