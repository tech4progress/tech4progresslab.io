# Security & Privacy

## Password Management

⚠️ **PLEASE USE THIS** ⚠️

In this day and age, it's so trivial to steal passwords. Check [Firefox
Monitor](https://monitor.firefox.com/) to see if—or how many—of your accounts
and passwords have already been compromised. Computers are fast enough to brute
force simple passwords, and websites get hacked often enough to leak at least
one copy of your credentials, opening the door to losing other accounts under
any similar username and passwords.

Your life will be so much _easier_ if you have one main password to unlock your
password manager, and generate random unique passwords for everything else. On
the other hand, if you still use variations on some memorable password like
`mypassword99`, just one account needs to get compromised to make it very easy
to fall victim to an attack.

⚠️ **SERIOUSLY, IT'S NOT WORTH WAITING TO GET HACKED** ⚠️

<big>Instead of: LastPass, 1Password, KeePass, etc.</big>

### Bitwarden

Bitwarden supports all major browsers and mobile phone operating systems, so you
can use unique, randomly generated passwords for each account you have, but only
have to remember one secure passphrase. If you are a Firefox user, you can also
configure your phone to use Firefox to automatically store and fill passwords
for you by [enabling system-wide autofill](
https://support.mozilla.org/en-US/kb/end-of-support-firefox-lockwise#w_enable-system-wide-autofill
).

[https://bitwarden.com/](https://bitwarden.com/)

[![Bitwarden](/assets/images/bitwarden.png "Bitwarden")](https://bitwarden.com/)

## VPN

A VPN can be useful to get around firewalls and hide your IP address from sites
you visit, and hide the sites you visit from your internet provider. They don't
provide very strong privacy, but they are useful to access sites from other
regions, or to download torrents.

<big>Instead of: NordVPN, ExpressVPN, CyberGhost, HideMyAss, Vypr, VPN
Unlimited, PureVPN, Surfshark, TunnelBear</big>

### Mozilla VPN

This is a great way to use [Mullvad](https://mullvad.net/en/) at a cheaper price
which means the best VPN service including Wireguard support.

[Mozilla VPN](https://www.mozilla.org/en-US/products/vpn/)

[![Mozilla VPN](/assets/images/mozilla-vpn.png "Mozilla VPN")](https://www.mozilla.org/en-US/products/vpn/)
