# Project Management

<big>Instead of: Trello, Asana, Google Sites' Wiki-mode, Todoist, Wunderlist,
Basecamp</big>

## Taiga

[https://taiga.io/](https://taiga.io/)

[![taiga.io](/assets/images/taiga.jpg "Taiga")](https://taiga.io)

Honorable mentions: [Loomio](https://www.loomio.org/)
