# Audio

## Hosting

Use these to share music or create a podcast.

Similar to: SoundCloud, Libsyn, Buzzsprout, Podbean, Simplecast

## Funkwhale

[https://funkwhale.audio/](https://funkwhale.audio/)

[![Funkwhale](/assets/images/funkwhale.jpg "Funkwhale")](https://funkwhale.audio/)

## Editing

# Ardour

[https://ardour.org/](https://ardour.org/)

[![Ardour](/assets/images/ardour.png "Ardour")](https://ardour.org/)

# Audacity

[https://www.audacityteam.org/](https://www.audacityteam.org/)
