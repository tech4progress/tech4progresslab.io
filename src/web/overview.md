# Web

## Utilities

### Kutt.it URL Shortening

<big>Instead of: Bit.ly</big>

[Kutt](https://kutt.it/)

[![kutt.it](/assets/images/kutt.png "Kutt")](https://kutt.it)

### SingleLink

<big>Instead of: Linktree</big>

[Singlelink](https://www.singlelink.co/)

[![singlelink.co](/assets/images/singlelink.png "Singlelink")](https://www.singlelink.co/)


### QR Code Generator

[QRBTF](https://qrbtf-com.translate.goog/?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en)
is nice, but only in Chinese! Otherwise, [Awesome QR](https://www.bitcat.cc/webapp/awesome-qr/index.html#customize) was great, but is currently offline.

[![qrbtf.com](/assets/images/qrbtf.png "QRBTF")](https://qrbtf-com.translate.goog/?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en)

## Domain

### Gandi

[https://gandi.net](https://gandi.net)
