# Collaborative Live Editing

<big>Instead of Google Docs, Live Office</big>

Want to collaborate without seeing edits in real-time? See the next section, on
[documents & publishing](/productivity/documents-publishing.html).

## Fidus Writer

[Fidus Writer](https://www.fiduswriter.com/)

Notes: Good for collaboration (account required), but no end-to-end encryption

## Turtl

[https://turtlapp.com/](https://turtlapp.com/)

Notes: Supports collaborative editing and end-to-end encryption, but free accounts are limited to a few collaborators.
