# Notes

<big>Instead of: Notion, Google Keep, Evernote, Microsoft OneNote</big>

## AppFlowy

- 🟢 Libre (Free Software, aka "Open Source")
- 🟢 Great design, solid stack
- 🟡 Hosting not provided

[AppFlowy](https://www.appflowy.io/)


[![appflowy.io](/assets/images/appflowy.png "Appflowy")](https://appflowy.io)

## Standard Notes

- 🟢 Libre (Free Software, aka "Open Source")
- 🟢 Great design, modern stack
- 🟢 No cost to use

[Standard Notes](https://standardnotes.com/)

[![standardnotes.com](/assets/images/standardnotes.png "Standard Notes")](https://standardnotes.com)

## Notabase

[Notabase](https://notabase.io/)

- 🟢 Libre (Free Software, aka "Open Source")
- 🟢 Great design, modern stack
- 🟢 No cost to use<sup>*</sup>

_<sup>*</sup> The 100-note limit is pretty restricting for the free plan_

[![notabase.io](/assets/images/notabase.webp "Notabase")](https://notabase.io)

# Documents & Publishing

## Documize Community

<big>Instead of Wikia, Google Docs, Nuclino</big>

- 🟢 Libre (Free Software, aka "Open Source")
- 🟢 Great design, solid stack
- 🟡 Hosting not provided

[Documize Community](https://www.documize.com/community/)

[![documize.com/community](/assets/images/documize%20community.png "Documize Community")](https://documize.com/community)

## LireOffice

<big>Instead of: Microsoft Office</big>

[LibreOffice](https://www.libreoffice.org/)

[![https://libreoffice.org](/assets/images/libreoffice.png "LibreOffice")](https://libreoffice.org)

## Editoria

[Editoria](https://editoria.community/)

[![https://editoria.community](/assets/images/editoria.svg "Editoria")](https://editoria.community)

## Manifold

[Manifold](https://manifoldapp.org/)

[![https://manifoldapp.org](/assets/images/editoria.svg "Manifold")](https://manifoldapp.org)
