# Spreadsheets

<big>Instead of: Airtable, Google Sheets</big>

## Grist

[Grist](https://www.getgrist.com/)

- 🟢 Libre (Free Software, aka "Open Source")
- 🟢 Great design, solid stack
- 🟢 Free to use

[![Grist](/assets/images/grist.png "Grist")](https://getgrist.com)

## Baserow

[Baserow](https://baserow.io/)

- 🟢 Libre (Free Software, aka "Open Source")
- 🟢 Great design, solid stack
- 🟢 Free to use

[![Baserow](/assets/images/baserow.png "Baserow")](https://baserow.io)
