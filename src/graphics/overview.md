# Graphics

## Design & Prototyping

<big>Instead of: Canva, Figma, Vectr</big>

### Penpot

Canva costs $120 per year for the ability to export into a standard format that
can be used in other applications. Figma charges $144 per year per user for
removing the 3 file limit. Penpot uses the standard .svg format and plays nice
with other tools, plus none of those limitations on free usage. The template
library is new and mostly focused on user interface design, but hopefully very
soon, general print design templates will be available too.

- 🟢 Libre (Free Software, aka "Open Source")
- 🟢 Great design, modern stack
- 🟢 No cost to use

[https://penpot.app/](https://penpot.app/)

[![Penpot.app](/assets/images/penpot.webp "Penpot")](https://penpot.app/)

## Vector Editing

<big>Instead of: Adobe Illustrator, CorelDRAW, Sketch</big>

### Inkscape

There are many areas where Inkscape excels over the alternatives, even though
Adobe has a strangle hold on the design industry, more and more people are
realizing that the high cost doesn't actually bring more powerful editing. There
are some nice tutorial [resources on YouTube](https://youtu.be/3TXX10LdrhA) too.

[https://inkscape.org](https://inkscape.org)

[![Inkscape.org](/assets/images/inkscape.png "Inkscape")](https://inkscape.org)

## Bitmap/Photo Editing

<big>Instead of: Photoshop</big>

### GNU IMP

[https://gimp.org](https://gimp.org)

[![gimp.org](/assets/images/gnu%20imp.png "GNU IMP")](https://gimp.org)

## Darktable

[https://darktable.org](https://darktable.org)

[![darktable.org](/assets/images/darktable.png "Darktable")](https://darktable.org)
