# Wishlist & Coming Soon

## Design Version Control

We need an libre version of https://www.abstract.com/, maybe based on the
[git-annex](https://git-annex.branchable.com/) project or something similar to
content-centric networking like https://named-data.net/ or IPFS.

## Peer-to-peer filesharing

We need something like popcorn-time for all kinds of media, including scholarly
articles, books, and music.
