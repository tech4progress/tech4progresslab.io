# Communication & Messaging

## Team Chat

<big>Instead of: Slack, Discord, Microsoft Teams, Google Chat</big>

### Element (Matrix)

Slack holds your conversation history hostage and charges for you to be able to
keep access to messages past the arbitrary cap. Discord charges to unlock HD
video. [Element](https://element.io/) runs on Matrix, which means you can use
the default server (we're talking actual servers, not what discord pretends is a
server) or you can choose another hosting provider, so competition keeps the
price down. Matrix is also the only one which provides end-to-end encryption.

[https://element.io/](https://element.io/)

- 🟢 Libre (Free Software, aka "Open Source")
- 🟢 Great design, modern stack
- 🟢 No cost to use

[![Element.io](/assets/images/element.png "Element")](https://element.io/)

Also check out: Signal messenger. Signal is good for one-to-one chats, but not
great when you have to manage a bunch of overlapping group chats.

## Videoconferencing

<big>Instead of: Zoom, Google Meet, Skype, Duo</big>

### Jitsi Meet

Zoom charges to hold group meetinngs longer than 40 minutes, or the ability to
stream to YouTube. Google's free limit is not buch better, an hour, for group
calls. [Jitsi Meet](https://meet.jit.si/) doesn't have any of these limits, and
supported end-to-end encryption early on (something Zoom was sued for lying
about).

Also, the paid-version of Jitsi Meet, [8×8.vc](https://8x8.vc/), lets
you add your own branding to the UI, and increases the simultaneous caller limit
to 500 per meeting, and only costs $0.99 per user per month. Zoom's lowest-tier
plan is $12.49/user/month, but to increase the caller limit that high costs an
additional $50 per month! Google's lowest-tier option is $7.99/user/month and is
still limited to 100 callers, without a set price on increasing to 500.

- 🟢 Libre (Free Software, aka "Open Source")
- 🟡 Great design, adequate stack<sup>*</sup>
- 🟢 No cost to use

_<sup>*</sup> Jitsi is very stable and secure, and the codebase isn't PHP, but
it is still untyped JavaScript._

[https://jitsi.org/jitsi-meet/](https://meet.jit.si/)

[![https://jitsi.org/jitsi-meet/](/assets/images/jitsi-meet.png "Jitsi Meet")](https://meet.jit.si/)
