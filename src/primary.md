### Honorable Mentions

* [Editoria](https://editoria.pub/)

## Fundraising

### Projects to watch

These aren't ready, but may be useful in the future

* [Snowdrift](https://snowdrift.coop/)

### Honorable mentions

* elm-pages

## Courses

### P2PU

## Live Polls

### Pingo

http://trypingo.com/

# Image Sharing

### PixelFed

## Hosting

### OVH

[https://us.ovhcloud.com](https://us.ovhcloud.com)
