# Fundraising

Similar to: GoFundMe, Indiegogo, Kickstarter

## CommitChange (Houdini)

[https://commitchange.com/](https://commitchange.com/)

Notes: Only for nonprofit orgs

## Goteo

[https://en.goteo.org/](https://en.goteo.org/)

## OpenCollective

[https://opencollective.com/](https://opencollective.com/)

Notes: Higher fees, encourages making budget/spending public and transparent
