# Technology for the Masses

<big>Choose your tools wisely with this collection of apps and websites to
maximize your efficacy, not monopolistic tech-company profits.</big>

✨ Under Construction ✨

## A curated list of tools for organizations, educators, and activists.

It's hard to find services, software, and products that are:
1. Not full of advertisements & [anti-features](https://www.digitalethics.org/essays/it-feature-it-bug-no-its-antifeature),
or meant to spy on you or resort to vendor lock-in
2. Usable and of overall high quality
3. Economical, providing actually good value (affordable if not free)

This can be very important for projects which can't afford to waste resources,
projects which attempt to avoid surveillance and media manipulation, or those
which otherwise just need some good recommendations without the influence of
advertising.

## What are the qualifications for the list?

It is subjective, but this compilation highlights the best tools which are also
a public good. Their ownership model means that they don't engage in
anticompetitive practices such as enforcing artificial scarcity for profit, but
the selection here aims to showcase projects which are on-par, exceeding, or
have real potential to exceed their proprietary (monopolistic) counterparts.

Concretely the rubric is three points:

* Is not, or does not push, proprietary software. _i.e. preference for [Free
Software](https://copyleft.org/guide/comprehensive-gpl-guidech2.html) (popularly
marketed as "Open Source")_
* Well-designed, intuitive, accessible, and otherwise easy to use
* Has a strong foundation or "good bones", built on modern technologies and
principles _(i.e. for the nerds this means functional languages preferred to
imperative; statically typed languages preferred to dynamically typed languages,
otherwise just not written in PHP)_

## Suggestions?

Feel free to open an issue via our [Gitlab
repository](https://gitlab.com/tech4progress/tech4progress.gitlab.io/-/issues)
(or [by email](mailto:incoming+tech4progress-tech4progress-gitlab-io-20266918-11ho2lgnt8qi0qah93uhnqmkp-issue@incoming.gitlab.com)). If you're a nerd and are familiar with git and markdown, feel
free to edit this site and submit a merge request!
